import { Camera, Color, Component, EffectAsset, Node, Slider, Sprite, UITransform, Vec2, _decorator } from 'cc';
import { BlurFilter, SnapshotFilter } from '../../../Scripts/SnapshotFilter';
import { SnapshotRender } from '../../../Scripts/SnapshotRender';
import GuassianData from './GuassianData';
const { ccclass, property } = _decorator;

@ccclass('NewComponent')
export class NewComponent extends Component {

    @property(Camera)
    mainCamera: Camera = null;
    @property(Sprite)
    rt: Sprite = null;
    size: Vec2 = new Vec2();

    @property(EffectAsset)
    gaussianShader: EffectAsset = null!;
    @property(EffectAsset)
    gaussianCppShader: EffectAsset = null!;
    @property(Node)
    snapshotCanvas: Node = null;

    private filters: SnapshotFilter[] = [];
    private filterIndex: number = -1;

    start() {
        this.size.set(this.rt.node.getComponent(UITransform).width, this.rt.node.getComponent(UITransform).height);

        let render = this.snapshotCanvas.getComponent(SnapshotRender);
        render.setSnapshot(this.mainCamera);

        this.filters.push(new BlurFilter("Blur (Full)", Color.WHITE, this.gaussianShader));
    }

    sliderW(slider:Slider) {
        GuassianData._BlurOffsetX = 2 * slider.progress / this.size.x;
    }

    sliderH(slider:Slider) {
        GuassianData._BlurOffsetY = 2 * slider.progress / this.size.y;
    }

    update(deltaTime: number) {
        let lastIndex = this.filterIndex;


        if (lastIndex < 0) {
            this.filterIndex = 0;
        }

        let filter = this.filters[this.filterIndex];

        let render = this.snapshotCanvas.getComponent(SnapshotRender);
        filter.OnRenderImage(render, render.src, render.dst);
    }
}

