// Copyright (c) 2017-2020 Xiamen Yaji Software Co., Ltd.
CCEffect %{
  techniques:
  - passes:
    - vert: sprite-vs:vert
      frag: sprite-fs:frag
      depthStencilState:
        depthTest: false
        depthWrite: false
      blendState:
        targets:
        - blend: true
          blendSrc: src_alpha
          blendDst: one_minus_src_alpha
          blendDstAlpha: one_minus_src_alpha
      rasterizerState:
        cullMode: none
      properties:
        alphaThreshold: { value: 0.5 }
        _BlurOffsetX: {value: 0,editor: { slide: true, range: [0, 1.0], step: 0.0001 }}
        _BlurOffsetY: {value: 0,editor: { slide: true, range: [0, 1.0], step: 0.0001 }}
}%

CCProgram sprite-vs %{
  precision highp float;
  #include <builtin/uniforms/cc-global>
  #if USE_LOCAL
    #include <builtin/uniforms/cc-local>
  #endif
  #if SAMPLE_FROM_RT
    #include <common/common-define>
  #endif
  in vec3 a_position;
  in vec2 a_texCoord;
  in vec4 a_color;

  out vec4 color;
  out vec2 uv0;


  vec4 vert () {
    vec4 pos = vec4(a_position, 1);

    #if USE_LOCAL
      pos = cc_matWorld * pos;
    #endif

    #if USE_PIXEL_ALIGNMENT
      pos = cc_matView * pos;
      pos.xyz = floor(pos.xyz);
      pos = cc_matProj * pos;
    #else
      pos = cc_matViewProj * pos;
    #endif

    uv0 = a_texCoord;
    #if SAMPLE_FROM_RT
      CC_HANDLE_RT_SAMPLE_FLIP(uv0);
    #endif
    color = a_color;

    return pos;
  }
}%

CCProgram sprite-fs %{
  precision highp float;
  #include <builtin/internal/embedded-alpha>
  #include <builtin/internal/alpha-test>

  in vec4 color;

  #if USE_TEXTURE
    in vec2 uv0;
    #pragma builtin(local)
    layout(set = 2, binding = 11) uniform sampler2D cc_spriteTexture;
  #endif

  uniform Constant{
    float _BlurOffsetX;
    float _BlurOffsetY;
  };

  // float offset[5];
  // float weight[5];

  // offset[0] = 0.0;
  // offset[1] = 1.0;
  // offset[2] = 2.0;
  // offset[3] = 3.0;
  // offset[4] = 4.0;

  // weight[0] = 0.2270270270;
  // weight[1] = 0.1945945946;
  // weight[2] = 0.1216216216;
  // weight[3] = 0.0540540541;
  // weight[4] = 0.0162162162;

  vec4 GaussianBlur() {
    	// 原点
      vec4 color = 0.2270270270 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0);
      // 右边/上方的采样点
      color += 0.1945945946 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0 + vec2(1.0 * _BlurOffsetX  , 1.0 * _BlurOffsetY ));
      color += 0.1216216216 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0 + vec2(2.0 * _BlurOffsetX  , 2.0 * _BlurOffsetY ));
      color += 0.0540540541 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0 + vec2(3.0 * _BlurOffsetX  , 3.0 * _BlurOffsetY ));
      color += 0.0162162162 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0 + vec2(4.0 * _BlurOffsetX  , 4.0 * _BlurOffsetY ));
      // 左边/下方的采样点
      color += 0.1945945946 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0 + vec2(-1.0 * _BlurOffsetX  , -1.0 * _BlurOffsetY ));
      color += 0.1216216216 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0 + vec2(-2.0 * _BlurOffsetX  , -2.0 * _BlurOffsetY ));
      color += 0.0540540541 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0 + vec2(-3.0 * _BlurOffsetX  , -3.0 * _BlurOffsetY ));
      color += 0.0162162162 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0 + vec2(-4.0 * _BlurOffsetX  , -4.0 * _BlurOffsetY ));
      
      return color;
  }

  vec4 GaussianBlurLinear9() {
    	vec4 color = 0.2270270270 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0);

      color += 0.3162162162 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0 + vec2(1.3846153846 * _BlurOffsetX  , 1.3846153846 * _BlurOffsetY ));
      color += 0.0702702703 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0 + vec2(3.2307692308 * _BlurOffsetX  , 3.2307692308 * _BlurOffsetY ));
      color += 0.3162162162 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0 + vec2(-1.3846153846 * _BlurOffsetX  , -1.3846153846 * _BlurOffsetY ));
      color += 0.0702702703 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0 + vec2(-3.2307692308 * _BlurOffsetX  , -3.2307692308 * _BlurOffsetY ));
      
      return color;
  }


  vec4 GaussianBlurLinear13() {
    	vec4 color = 0.1964825501511404 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0);

      color += 0.2969069646728344 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0 + vec2(1.411764705882353 * _BlurOffsetX  ,1.411764705882353 * _BlurOffsetY ));
      color += 0.2969069646728344 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0 + vec2(-1.411764705882353 * _BlurOffsetX  , -1.411764705882353 * _BlurOffsetY ));
      color += 0.09447039785044732 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0 + vec2(3.2941176470588234 * _BlurOffsetX  , 3.2941176470588234 * _BlurOffsetY ));
      color += 0.09447039785044732 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0 + vec2(-3.2941176470588234 * _BlurOffsetX  , -3.2941176470588234 * _BlurOffsetY ));
      
      color += 0.010381362401148057 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0 + vec2(5.176470588235294 * _BlurOffsetX  , 5.176470588235294 * _BlurOffsetY ));
      color += 0.010381362401148057 * CCSampleWithAlphaSeparated(cc_spriteTexture, uv0 + vec2(-5.176470588235294 * _BlurOffsetX  , -5.176470588235294 * _BlurOffsetY ));
      return color;
  }


  vec4 frag () {
    vec4 o = vec4(1, 1, 1, 1);

    #if USE_TEXTURE
      o *= GaussianBlurLinear9();
      #if IS_GRAY
        float gray  = 0.2126 * o.r + 0.7152 * o.g + 0.0722 * o.b;
        o.r = o.g = o.b = gray;
      #endif
    #endif

    o *= color;
    ALPHA_TEST(o);
    return o;
  }
}%
