
import { _decorator, Component, Node, Label, Material, Color, EffectAsset, RenderTexture, find, CCObject, gfx, Texture2D, builtinResMgr, renderer, director, Director, game, Vec4 } from 'cc';
import GuassianData from '../case/blur/Gaussian/GuassianData';
import { SnapshotRender } from './SnapshotRender';

export abstract class SnapshotFilter {
    protected mainMaterial: Material = null;
    protected name: string = "";
    protected color: Color = new Color();

    constructor(name: string, color: Color, shader: EffectAsset) {
        this.name = name;
        this.color = color;

        this.mainMaterial = new Material();
        this.mainMaterial.initialize({ effectName: name, effectAsset: shader });
    }

    abstract OnRenderImage(render: SnapshotRender, src: RenderTexture, dst: RenderTexture);

    public GetName(): string {
        return this.name;
    }

    public GetColor(): Color {
        return this.color;
    }
}

export class BaseFilter extends SnapshotFilter {
    pass0: renderer.MaterialInstance;
    needDepth: boolean = false;

    constructor(name: string, color: Color, shader: EffectAsset, needDepth?: boolean) {
        super(name, color, shader);
        this.needDepth = needDepth;
        this.pass0 = new renderer.MaterialInstance({ parent: this.mainMaterial });
        this.pass0.recompileShaders({
            USE_TEXTURE: true,
            SAMPLE_FROM_RT: true
        }, 0);
    }

    OnRenderImage(render: SnapshotRender, src: RenderTexture, dst: RenderTexture) {
        render.blit(src, dst, this.pass0, this.needDepth);
    }
}

export class BlurFilter extends SnapshotFilter {
    pass0: renderer.MaterialInstance;
    pass1: renderer.MaterialInstance;
    pass2: renderer.MaterialInstance;
    pass3: renderer.MaterialInstance;
    pass4: renderer.MaterialInstance;
    pass5: renderer.MaterialInstance;
    pass6: renderer.MaterialInstance;
    pass7: renderer.MaterialInstance;
    constructor(name: string, color: Color, shader: EffectAsset) {
        super(name, color, shader);
        this.pass0 = new renderer.MaterialInstance({ parent: this.mainMaterial });
        this.pass0.recompileShaders({
            USE_TEXTURE: true,
            SAMPLE_FROM_RT: true
        }, 0);
        this.pass1 = new renderer.MaterialInstance({ parent: this.mainMaterial });
        this.pass1.recompileShaders({
            USE_TEXTURE: true,
            SAMPLE_FROM_RT: true,
            // USE_VERTICAL: true
        }, 0);
        this.pass2 = new renderer.MaterialInstance({ parent: this.mainMaterial });
        this.pass2.recompileShaders({
            USE_TEXTURE: true,
            SAMPLE_FROM_RT: true
        }, 0);
        this.pass3 = new renderer.MaterialInstance({ parent: this.mainMaterial });
        this.pass3.recompileShaders({
            USE_TEXTURE: true,
            SAMPLE_FROM_RT: true,
            // USE_VERTICAL: true
        }, 0);
        this.pass4 = new renderer.MaterialInstance({ parent: this.mainMaterial });
        this.pass4.recompileShaders({
            USE_TEXTURE: true,
            SAMPLE_FROM_RT: true,
            // USE_VERTICAL: true
        }, 0);
        this.pass5 = new renderer.MaterialInstance({ parent: this.mainMaterial });
        this.pass5.recompileShaders({
            USE_TEXTURE: true,
            SAMPLE_FROM_RT: true,
            // USE_VERTICAL: true
        }, 0);
        this.pass6 = new renderer.MaterialInstance({ parent: this.mainMaterial });
        this.pass6.recompileShaders({
            USE_TEXTURE: true,
            SAMPLE_FROM_RT: true,
            // USE_VERTICAL: true
        }, 0);
        this.pass7 = new renderer.MaterialInstance({ parent: this.mainMaterial });
        this.pass7.recompileShaders({
            USE_TEXTURE: true,
            SAMPLE_FROM_RT: true,
            // USE_VERTICAL: true
        }, 0);
    }

    OnRenderImage(render: SnapshotRender, src: RenderTexture, dst: RenderTexture) {
        // Create a temporary RenderTexture to hold the first pass.
        let tmp1: RenderTexture = render.temporary(src.width, src.height, src.getPixelFormat());
        let tmp2: RenderTexture = render.temporary(src.width, src.height, src.getPixelFormat());
        let tmp3: RenderTexture = render.temporary(src.width, src.height, src.getPixelFormat());
        let tmp4: RenderTexture = render.temporary(src.width, src.height, src.getPixelFormat());
        let tmp5: RenderTexture = render.temporary(src.width, src.height, src.getPixelFormat());
        let tmp6: RenderTexture = render.temporary(src.width, src.height, src.getPixelFormat());
        let tmp7: RenderTexture = render.temporary(src.width, src.height, src.getPixelFormat());

        this.pass0.setProperty('_BlurOffsetX', GuassianData._BlurOffsetX)
        this.pass0.setProperty('_BlurOffsetY', 0)

        this.pass1.setProperty('_BlurOffsetX', 0)
        this.pass1.setProperty('_BlurOffsetY', GuassianData._BlurOffsetY)

        this.pass2.setProperty('_BlurOffsetX', GuassianData._BlurOffsetX)
        this.pass2.setProperty('_BlurOffsetY', 0)

        this.pass3.setProperty('_BlurOffsetX', 0)
        this.pass3.setProperty('_BlurOffsetY', GuassianData._BlurOffsetY)

        this.pass4.setProperty('_BlurOffsetX', GuassianData._BlurOffsetX)
        this.pass4.setProperty('_BlurOffsetY', 0)

        this.pass5.setProperty('_BlurOffsetX', 0)
        this.pass5.setProperty('_BlurOffsetY', GuassianData._BlurOffsetY)

        this.pass6.setProperty('_BlurOffsetX', GuassianData._BlurOffsetX)
        this.pass6.setProperty('_BlurOffsetY', 0)

        this.pass7.setProperty('_BlurOffsetX', 0)
        this.pass7.setProperty('_BlurOffsetY', GuassianData._BlurOffsetY)

        // // Perform both passes in order.
        render.blit(src, tmp1, this.pass0);   // 1 pass.
        render.blit(tmp1, tmp2, this.pass1);   // 2 pass.
        render.blit(tmp2, tmp3, this.pass2);   // 3 pass.
        render.blit(tmp3, tmp4, this.pass3);   // 4 pass.
        render.blit(tmp4, tmp5, this.pass4);   // 4 pass.
        render.blit(tmp5, tmp6, this.pass5);   // 4 pass.
        render.blit(tmp6, tmp7, this.pass6);   // 4 pass.
        render.blit(tmp7, dst, this.pass7);   // 4 pass.

        render.releaseTemporary(tmp1);
        render.releaseTemporary(tmp2);
        render.releaseTemporary(tmp3);
        render.releaseTemporary(tmp4);
        render.releaseTemporary(tmp5);
        render.releaseTemporary(tmp6);
        render.releaseTemporary(tmp7);
    }
}

export class BlurCppFilter extends SnapshotFilter {
    pass0: renderer.MaterialInstance;
    constructor(name: string, color: Color, shader: EffectAsset) {
        super(name, color, shader);
        this.pass0 = new renderer.MaterialInstance({ parent: this.mainMaterial });
        this.pass0.recompileShaders({
            USE_TEXTURE: true,
            SAMPLE_FROM_RT: true
        }, 0);
    }

    OnRenderImage(render: SnapshotRender, src: RenderTexture, dst: RenderTexture) {
        // Create a temporary RenderTexture to hold the first pass.
        this.pass0.setProperty('_BlurOffsetX', GuassianData._BlurOffsetX)
        this.pass0.setProperty('_BlurOffsetY', GuassianData._BlurOffsetY)

        // // Perform both passes in order.
        render.blit(src, dst, this.pass0);   // 1 pass.
    }

}

export class BloomFilter extends SnapshotFilter {
    pass0: renderer.MaterialInstance;
    pass1: renderer.MaterialInstance;

    private blur: BlurFilter = null;
    // IDs of each pass inside the shader.
    private thresholdPass = 0;
    private horizontalPass = 1;
    private verticalPass = 2;
    private bloomPass = 3;

    constructor(name: string, color: Color, shader: EffectAsset, blur: BlurFilter) {
        super(name, color, shader);
        this.blur = blur;

        this.pass0 = new renderer.MaterialInstance({ parent: this.mainMaterial });
        this.pass0.recompileShaders({
            USE_TEXTURE: true,
            SAMPLE_FROM_RT: true
        }, 0);
        this.pass0.setProperty('threshold', 0.58);

        this.pass1 = new renderer.MaterialInstance({ parent: this.mainMaterial });
        this.pass1.recompileShaders({
            USE_TEXTURE: true,
            SAMPLE_FROM_RT: true,
            USE_PASS2: true,
        }, 0);
    }

    OnRenderImage(render: SnapshotRender, src: RenderTexture, dst: RenderTexture) {
        // Create a temporary RenderTexture to hold the first pass.
        let thresholdTex: RenderTexture = render.temporary(src.width, src.height, src.getPixelFormat());
        render.blit(src, thresholdTex, this.pass0);

        let blurTex: RenderTexture = render.temporary(src.width, src.height, src.getPixelFormat());
        this.blur.OnRenderImage(render, thresholdTex, blurTex);
        render.releaseTemporary(thresholdTex);

        this.pass1.setProperty("srcTexture", src);
        render.blit(blurTex, dst, this.pass1);
        render.releaseTemporary(blurTex);
    }
}

export class NeonFilter extends BaseFilter {
    pass0: renderer.MaterialInstance;
    private bloom: BloomFilter = null;

    constructor(name: string, color: Color, shader: EffectAsset, bloom: BloomFilter) {
        super(name, color, shader);
        this.bloom = bloom;

        this.pass0 = new renderer.MaterialInstance({ parent: this.mainMaterial });
        this.pass0.recompileShaders({
            USE_TEXTURE: true,
            SAMPLE_FROM_RT: true
        }, 0);
    }

    OnRenderImage(render: SnapshotRender, src: RenderTexture, dst: RenderTexture) {
        let tmp: RenderTexture = render.temporary(src.width, src.height, src.getPixelFormat());
        if (this.bloom) {
            render.blit(src, tmp, this.pass0);
            this.bloom.OnRenderImage(render, tmp, dst);
        }
        else {
            render.blit(src, dst, this.pass0);
        }
        render.releaseTemporary(tmp);
    }
}

export class PixelFilter extends SnapshotFilter {
    private pixelSize = 3;
    private pass0: renderer.MaterialInstance;
    private pass1: renderer.MaterialInstance;

    constructor(name: string, color: Color, shader: EffectAsset) {
        super(name, color, shader);
        this.pass0 = new renderer.MaterialInstance({ parent: builtinResMgr.get(`ui-sprite-material`) });
        this.pass0.recompileShaders({
            USE_TEXTURE: true,
            SAMPLE_FROM_RT: true,
        }, 0);
        this.pass1 = new renderer.MaterialInstance({ parent: this.mainMaterial });
        this.pass1.recompileShaders({
            USE_TEXTURE: true,
            SAMPLE_FROM_RT: true,
            USE_VERTICAL: true
        }, 0);
    }

    OnRenderImage(render: SnapshotRender, src: RenderTexture, dst: RenderTexture) {
        let width = src.width / this.pixelSize;
        let height = src.height / this.pixelSize;

        let tmp: RenderTexture = render.temporary(width, height, src.getPixelFormat());

        // Make sure the upsampling does not interpolate.
        src.setFilters(Texture2D.Filter.NEAREST, Texture2D.Filter.NEAREST);

        // Obtain a smaller version of the source input.
        render.blit(src, tmp, this.pass0);
        render.blit(tmp, dst, this.pass1);

        render.releaseTemporary(tmp);
    }
}

export class CRTFilter extends BaseFilter {
    private pixelFilter: PixelFilter = null!;
    private brightness = 27.0;
    private contrast = 2.1;

    constructor(name: string, color: Color, shader: EffectAsset, pixelFilter: PixelFilter) {
        super(name, color, shader);
        this.pixelFilter = pixelFilter;

        this.pass0 = new renderer.MaterialInstance({ parent: this.mainMaterial });
        this.pass0.recompileShaders({
            USE_TEXTURE: true,
            SAMPLE_FROM_RT: true,
        }, 0);
        this.pass0.setProperty("_Brightness", this.brightness);
        this.pass0.setProperty("_Contrast", this.contrast);
    }

    OnRenderImage(render: SnapshotRender, src: RenderTexture, dst: RenderTexture) {
        let tmp: RenderTexture = render.temporary(src.width, src.height, src.getPixelFormat());
        this.pixelFilter.OnRenderImage(render, src, tmp);
        render.blit(tmp, dst, this.pass0);
        render.releaseTemporary(tmp);
    }
}
